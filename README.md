# Finite State Machine

# Installation 

Ensure you have a fairly recent version of NodeJs - https://nodejs.org/en/.

```
npm i
npm run build
```

# Run program 

```
npm run start -- --alphabet 110
npm run start -- -a 1010
npm run start -- -a 1badinput // expected to throw an error
```

# Run tests 

```
npm run test
```

with code coverage
```
npm run coverage 
```

# Generate documentation 

```
npm run docs
```

# Run linter

```
npm run lint 
```
