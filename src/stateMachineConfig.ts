import IStateMachineConfig from "./typings/IStateMachineConfig";

const config: IStateMachineConfig = {
  startingState: "S0",
  states: {
    S0: {
      isAcceptingState: true,
      value: 0,
      transitions: {
        "0": {
          action: (): string => {
            return "S0";
          }
        },
        "1": {
          action: (): string => {
            return "S1";
          }
        }
      }
    },
    S1: {
      isAcceptingState: true,
      value: 1,
      transitions: {
        "0": {
          action: (): string => {
            return "S2";
          }
        },
        "1": {
          action: (): string => {
            return "S0";
          }
        }
      }
    },
    S2: {
      isAcceptingState: true,
      value: 2,
      transitions: {
        "0": {
          action: (): string => {
            return "S1";
          }
        },
        "1": {
          action: (): string => {
            return "S2";
          }
        }
      }
    }
  }
};

export default config;