import IState from "../typings/IState";
import IStateMachineConfig, {
  IStateConfig,
  IStateTransition
} from "../typings/IStateMachineConfig";
import IStateMachine from "../typings/IStateMachine";

/**
 * This class implements the state machine.
 */
export default class StateMachine implements IStateMachine {
  /**
   * State machine configuration
   */
  private config: IStateMachineConfig;

  /**
   * Constructor for state machine
   *
   * @param config State machine configuration
   */
  constructor(config: IStateMachineConfig) {
    this.config = config;
  }

  /**
   * Run the state machine against an alphabet
   *
   * @param alphabet The alphabet
   * @returns The final state
   */
  public run(alphabet: string): IState {
    let stateName = this.config.startingState;
    const stateConfig: IStateConfig = this.getStateConfig(stateName);
    let currentState: IState = {
      name: stateName,
      value: stateConfig.value,
      isAcceptingState: stateConfig.isAcceptingState
    };

    for (const item of alphabet) {
      currentState = this.nextState(stateName, item);
      stateName = currentState.name;
    }

    return currentState;
  }

  /**
   * Generate the next state given the current state and an input
   * alphabet character.
   *
   * @param currentState The current state
   * @param input An input character of an alphabet
   * @returns The new state
   */
  public nextState(currentState: string, input: string): IState {
    const transitionConfig = this.getStateTransitionConfig(currentState, input);
    const newStateName = transitionConfig.action();
    const newStateConfig: IStateConfig = this.getStateConfig(newStateName);

    return {
      name: newStateName,
      value: newStateConfig.value,
      isAcceptingState: newStateConfig.isAcceptingState
    };
  }

  /**
   * Obtain the state configuration given the name of a state.
   *
   * @param state A state name
   * @returns The configuration associated with the state name
   */
  private getStateConfig(state: string): IStateConfig {
    const stateConfig: IStateConfig = this.config.states[state];
    if (!stateConfig) {
      throw new Error(`Unknown state: ${state}`);
    }
    return stateConfig;
  }

  /**
   * Obtain the state transition configuration given the current state and
   * an input alphabet character
   *
   * @param currentState The current state
   * @param input The input alphabet character
   * @returns The state transition configuration
   */
  private getStateTransitionConfig(
    currentState: string,
    input: string
  ): IStateTransition {
    const currentStateConfig: IStateConfig = this.getStateConfig(currentState);
    const transitionConfig = currentStateConfig.transitions[input];
    if (!transitionConfig) {
      throw new Error(
        `No available transition from current state ${currentState} given input of ${input}`
      );
    }
    return transitionConfig;
  }
}
