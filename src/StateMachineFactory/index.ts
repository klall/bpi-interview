import StateMachine from '../StateMachine/index';
import IStateMachine from '../typings/IStateMachine';
import IStateMachineConfig from '../typings/IStateMachineConfig';

/**
 * This class is a factory used to generate a state machine instance
 */
export default class StateMachineFactory {
  /**
   * Create the state machine for the given configuration
   *
   * @param config The state machine configuration
   * @returns An instance of the state machine 
   */
  static create(config: IStateMachineConfig): IStateMachine {
    return new StateMachine(config);
  }
}
