import IStateMachine from "../typings/IStateMachine";
import StateMachineFactory from "../StateMachineFactory";
import IStateMachineConfiguration from "../typings/IStateMachineConfig";
import IState from "../typings/IState";

describe("State Machine", () => {
  describe("Given a state machine configuration", () => {
    describe("And it is an invalid configuration", () => {
      describe("When no states are provided", () => {
        it("Then it should throw an error", () => {
          const config: IStateMachineConfiguration = {
            startingState: "S0",
            states: {}
          };
          const stateMachine = StateMachineFactory.create(config);
          expect(() =>
            stateMachine.nextState(config.startingState, "1")
          ).toThrow();
        });
      });
      describe("When no valid transition is available for a given input", () => {
        it("Then it should throw an error", () => {
          const config: IStateMachineConfiguration = {
            startingState: "S0",
            states: {
              S0: {
                isAcceptingState: true,
                value: 5,
                transitions: {}
              }
            }
          };
          const stateMachine = StateMachineFactory.create(config);
          expect(() =>
            stateMachine.nextState(config.startingState, "1")
          ).toThrow();
        });
      });
      describe("When there is a transition to a non-existing state", () => {
        it("Then it should throw an error", () => {
          const config: IStateMachineConfiguration = {
            startingState: "S0",
            states: {
              S0: {
                isAcceptingState: true,
                value: 5,
                transitions: {
                  "0": {
                    action: (): string => {
                      return "S99";
                    }
                  }
                }
              }
            }
          };
          const stateMachine = StateMachineFactory.create(config);
          expect(() =>
            stateMachine.nextState(config.startingState, "0")
          ).toThrow();
        });
      });
    });
    describe("And it is a valid configuration", () => {
      let stateMachine: IStateMachine;
      const config: IStateMachineConfiguration = {
        startingState: "S0",
        states: {
          S0: {
            isAcceptingState: true,
            value: 0,
            transitions: {
              "0": {
                action: (): string => {
                  return "S0";
                }
              },
              "1": {
                action: (): string => {
                  return "S1";
                }
              }
            }
          },
          S1: {
            isAcceptingState: true,
            value: 1,
            transitions: {
              "0": {
                action: (): string => {
                  return "S2";
                }
              },
              "1": {
                action: (): string => {
                  return "S0";
                }
              }
            }
          },
          S2: {
            isAcceptingState: true,
            value: 2,
            transitions: {
              "0": {
                action: (): string => {
                  return "S1";
                }
              },
              "1": {
                action: (): string => {
                  return "S2";
                }
              }
            }
          }
        }
      };

      describe("When the state machine is created", () => {
        it("Then the instance should exist", () => {
          stateMachine = StateMachineFactory.create(config);
          expect(stateMachine).toBeTruthy();
        });
      });

      describe("When the starting state is S0", () => {
        const machineConfig = Object.assign({}, config, {
          startingState: "S0"
        });
        beforeEach(() => {
          stateMachine = StateMachineFactory.create(machineConfig);
        });

        it("Then an alphabet character of 1 should transition to state S1", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "1"
          );

          expect(newState.name).toEqual("S1");
          expect(newState.value).toEqual(1);
          expect(newState.isAcceptingState).toEqual(true);
        });

        it("Then an alphabet character of 0 should transition to state S0", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "0"
          );

          expect(newState.name).toEqual("S0");
          expect(newState.value).toEqual(0);
          expect(newState.isAcceptingState).toEqual(true);
        });
      });

      describe("When an invalid alphabet character is provided", () => {
        const badInput = "x";
        const machineConfig = Object.assign({}, config, {
          startingState: "S0"
        });
        beforeEach(() => {
          stateMachine = StateMachineFactory.create(machineConfig);
        });

        it("Then the state machine should throw an error", () => {
          expect(() =>
            stateMachine.nextState(machineConfig.startingState, badInput)
          ).toThrow();
        });
        describe("And When provided with subsequent valid input", () => {
          it("Then it should successfully transition from the previously valid state", () => {
            const newState = stateMachine.nextState(
              machineConfig.startingState,
              "1"
            );
            expect(newState.name).toEqual("S1");
            expect(newState.value).toEqual(1);
            expect(newState.isAcceptingState).toEqual(true);
          });
        });
      });

      describe("When transitioning to a non-accepted state", () => {
        const machineConfig = {
          startingState: "S0",
          states: {
            S0: {
              isAcceptingState: true,
              value: 0,
              transitions: {
                "1": {
                  action: (): string => {
                    return "S1";
                  }
                }
              }
            },
            S1: {
              isAcceptingState: false,
              value: 1,
              transitions: {
                "0": {
                  action: (): string => {
                    return "S1";
                  }
                }
              }
            }
          }
        };
        beforeEach(() => {
          stateMachine = StateMachineFactory.create(machineConfig);
        });

        it("Then it should successfully transition", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "1"
          );
          expect(newState.name).toEqual("S1");
          expect(newState.value).toEqual(1);
          expect(newState.isAcceptingState).toEqual(false);
        });
      });

      describe("When the starting state is S1", () => {
        const machineConfig = Object.assign({}, config, {
          startingState: "S1"
        });
        beforeEach(() => {
          stateMachine = StateMachineFactory.create(config);
        });

        it("Then an alphabet character of 1 should transition to state S0", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "1"
          );

          expect(newState.name).toEqual("S0");
          expect(newState.value).toEqual(0);
          expect(newState.isAcceptingState).toEqual(true);
        });

        it("Then an alphabet character of 0 should transition to state S2", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "0"
          );

          expect(newState.name).toEqual("S2");
          expect(newState.value).toEqual(2);
          expect(newState.isAcceptingState).toEqual(true);
        });
      });

      describe("When the starting state is S2", () => {
        const machineConfig = Object.assign({}, config, {
          startingState: "S2"
        });
        beforeEach(() => {
          stateMachine = StateMachineFactory.create(config);
        });

        it("Then an alphabet character of 1 should transition to state S1", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "1"
          );

          expect(newState.name).toEqual("S2");
          expect(newState.value).toEqual(2);
          expect(newState.isAcceptingState).toEqual(true);
        });

        it("Then an alphabet character of 0 should transition to state S2", () => {
          const newState = stateMachine.nextState(
            machineConfig.startingState,
            "0"
          );

          expect(newState.name).toEqual("S1");
          expect(newState.value).toEqual(1);
          expect(newState.isAcceptingState).toEqual(true);
        });
      });

      describe("When given a valid series of alphabet characters", () => {
        describe("And the alphabet series is 110", () => {
          const alphabet = "110";

          it("Then the state machine should be in state S0 with value 0", () => {
            stateMachine = StateMachineFactory.create(config);
            const state: IState = stateMachine.run(alphabet);
            expect(state.name).toEqual("S0");
            expect(state.value).toEqual(0);
            expect(state.isAcceptingState).toEqual(true);
          });
        });

        describe("And the alphabet series is 1010", () => {
          const alphabet = "1010";

          it("Then the state machine should be in state S1", () => {
            stateMachine = StateMachineFactory.create(config);
            const state: IState = stateMachine.run(alphabet);
            expect(state.name).toEqual("S1");
            expect(state.isAcceptingState).toEqual(true);
          });
        });
      });

      describe("When given an invalid series of alphabet characters", () => {
        describe("And the alphabet series is 1x0", () => {
          const alphabet = "1x0";
          it("Then the state machine should throw an error", () => {
            stateMachine = StateMachineFactory.create(config);
            expect(() => stateMachine.run(alphabet)).toThrow();
          });
        });
        describe("And the alphabet series is empty", () => {
          const alphabet = "";

          it("Then the state machine should return its previous state", () => {
            stateMachine = StateMachineFactory.create(config);
            const state: IState = stateMachine.run(alphabet);
            expect(state.name).toEqual("S0");
            expect(state.value).toEqual(0);
            expect(state.isAcceptingState).toEqual(true);
          });
        });
      });
    });
  });
});
