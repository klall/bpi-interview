/**
 * Interface for a given state's configuration 
 */
export interface IStateConfig {
  isAcceptingState: boolean; // True if it is an accepting state, false otherwise
  value: unknown; // Value of the state
  transitions: IStateTransitions; // Transitions supported by the state
}

/**
 * Interface for a state's transition details
 */
export interface IStateTransition {
  action(): string; // A function for a state transition
}

/**
 * Interface for a state's transitions 
 */
export interface IStateTransitions {
  [key: string]: IStateTransition; // Available transitions
}

/**
 * Interface for configurations of states
 */
export interface IStateConfigs {
  [key: string]: IStateConfig; // Configurations for states
}

/**
 * Interface for the state machine configuration
 */
export default interface IStateMachineConfig {
  startingState: string;
  states: IStateConfigs;
}
