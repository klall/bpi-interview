import IState from "./IState";

/**
 * Interface for the state machine
 */
export default interface IStateMachine {
  run(alphabet: string): IState;
  nextState(currentState: string, input: string): IState;
}

