
/**
 * Interface for a given state
 */
export default interface IState {
  name: string; // Name of the state
  value: unknown; // Value of the state
  isAcceptingState: boolean; // True if it is an accepting state, false otherwise
}

