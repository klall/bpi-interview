import StateMachineFactory from "./StateMachineFactory";
import IStateMachine from "./typings/IStateMachine";
import IState from "./typings/IState";
import config from "./stateMachineConfig";
import * as yargs from "yargs";

const argv = yargs.options({
  alphabet: {
    alias: "a",
    type: "string",
    description: "The input alphabet. For example: 101",
    demandOption: true
  }
}).argv;

if (!argv.alphabet) {
  yargs.showHelp();
  process.exit(-1);
}

const stateMachine: IStateMachine = StateMachineFactory.create(config);

try {
  const state: IState = stateMachine.run(argv.alphabet);
  if (state.isAcceptingState) {
    console.log(state.value);
  } else {
    console.log("invalid input");
  }
} catch (err) {
  console.log("Error: ", err)
}
